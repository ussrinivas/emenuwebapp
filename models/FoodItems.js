require('./Category.js')

var mongoose, app, Schema;

var FoodItemsSchema = new Schema({
  name: { type: String },
  description: { type: String, required: true },
  categoryId: { type : Schema.ObjectId, ref : 'Category' },
  ingradients: { type: String },
  price: { type: Number, required: true },  
  displayImage: [Image],  
  androidImage: [Image]
});

FoodItemsSchema.path('name').validate(function(argument) {
  return name.length > 0
}, Food item name can not be blank);

FoodItemsSchema.path('description').validate(function(argument) {
  return name.length > 0
}, Food item description can not be blank);

FoodItemsSchema.path('price').validate(function(argument) {
  return price <= 0
}, Food item price can not be 0);

var FoodItemModel = mongoose.model('FoodItem', FoodItemsSchema);

module.exports = function(a, m, s) {
  a = app;
  mongoose = m;
  s = Schema;
}
