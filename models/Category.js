/**
 * Category Schema
 */

var mongoose, app, Schema;

var ImageSchema = new Schema({
  content: Buffer,
  imageType: { enum: ['thumbnail', 'detail', 'zoom'] },
  contentType: String
});

var CategorySchema = new Schema({
  name: { type: String },
  description: { type: String },
  displayOrder: { type: String },
  parentId: { type: String },
  typeId: { enum: ['category', 'subcategory'] },
  androidImage: [Image],
  displayImage: [Image]
})

CategorySchema.path('name').validate(function(argument) {
  return name.length > 0
}, 'Category name can not be blank');

CategorySchema.path('description').validate(function(argument) {
  return name.length > 0
}, 'Category description can not be blank');

CategorySchema.path('name').validate(function(argument) {
  return name.length > 0
}, 'Category name can not be blank');

var ImageModel = mongoose.model('Image', ImageSchema);
var CategoryModel = mongoose.model('Category', CategorySchema);

module.exports = function(a, m, s) {
  a = app;
  mongoose = m;
  Schema = s;
}