var LoungeApp = angular.module('LoungeApp', ["ngResource"]).config(function($routeProvider) {
    $routeProvider.when('/', {
        controller: HomeCtrl
    }).
    when('/categories', {
        controller: CategoriesCtrl
    }).
    when('/categories/add', {
        controller: CategoriesAddCtrl
    }).
    when('/food_items', {
        controller: FoodItemsCtrl
    }).
    when('/tables', {
        controller: TablesCtrl
    }).
    when('/orders', {
        controller: OrdersCtrl
    }).
    otherwise({
        redirectTo: '/home',
        controller: HomeCtrl
    });
});

LoungeApp.factory('LoungeAjax', function($resource) {
    return $resource('api/categories/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        }
    });
});

var HomeCtrl = function($scope, $location) {
    $scope.setRoute = function(route) {
        $location.path(route);
    }
}

var CategoriesAddCtrl = function($scope, $location, LoungeAjax, $http) {
    $scope.subcat = false;
    $scope.url = "/api/categories";
    // $scope.items = LoungeAjax.query();

    $scope.getItems = function() {
        $http.get($scope.url).success(function(data, status) {
            $scope.items = data;
        }).error(function(status, response) {
            alert('Failed to retrive elements');
        });
    }

    $scope.getItems();

    $scope.addCategory = function() {
        if ($scope.subcat){
            $scope.categoryForm.typeId = 2;
            $scope.categoryForm.displayOrder = 0;
        }
        else {
            $scope.categoryForm.typeId = 1;
            $scope.categoryForm.parentId = 0;
        }
        $http.post($scope.url, $scope.categoryForm).success(function(data, status) {
            // alert("Status" + status);
            $scope.items = data;
            $scope.categoryForm.name = '';
            $scope.categoryForm.description = '';
            $scope.subcat = false;
        }).error(function(data, status) {
            // alert("Request failed: " + status);
        });
    }
};

var CategoriesEditCtrl = function($scope, $location, LoungeAjax, $http) {
    $scope.subcat = false;
    $scope.cat_id = $('#cat_id').val();
    $scope.url = "/api/categories/" + $scope.cat_id;
    // $scope.items = LoungeAjax.query();
    $scope.categoryForm = [{name: '', displayImage:'', androidImage:'', displayOrder: '', description: '', parentId:''}];

    $scope.getItems = function() {
        $http.get($scope.url).success(function(data, status) {
            $scope.items = data;
            $scope.categoryForm.name = $scope.items[0].name;
            $scope.categoryForm.description = $scope.items[0].description;
            $scope.categoryForm.displayOrder = $scope.items[0].displayOrder;
            $scope.categoryForm.parentId = $scope.items[0].parentId;
            if($scope.items[0].typeId == 2) {
                $scope.subcat = true;
            }
        }).error(function(status, response) {
            alert('Failed to retrive elements');
        });
    }

    $scope.getItems();

    $scope.addCategory = function() {
        if ($scope.subcat){
            $scope.categoryForm.typeId = 2; //Sub-category of other category
            $scope.categoryForm.displayOrder = 0;
        }
        else {
            $scope.categoryForm.typeId = 1; // Parent category
            $scope.categoryForm.parentId = 0;
        }
        $http.post($scope.url, $scope.categoryForm).success(function(data, status) {
            // alert("Status" + status);
            $scope.items = data;
            $scope.categoryForm.name = '';
            $scope.categoryForm.description = '';
            $scope.subcat = false;
        }).error(function(data, status) {
            // alert("Request failed: " + status);
        });
    }
};

var CategoriesCtrl = function($scope, $location, LoungeAjax, $http) {
    $scope.subcat = false;
    $scope.cat = "Categories List";
    $scope.url = "/api/categories";
    $scope.create_form = false;
    $scope.filters = [
        { name: 'Alphabetical', value: 'Alphabetical'},
        { name: 'Categories', value: 'Categories'}
    ];
    // $scope.displayFilter = $scope.filters[0];

    // var $scope.items = getItems();

    $scope.getItems = function() {
        $http.get($scope.url).success(function(data, status) {
            $scope.items = data;
        }).error(function(status, response) {
            alert('Failed to retrive elements');
        });
    }

    $scope.getItems();
    $scope.editCategory = function(item) {

    }

    $scope.displayForm = function() {
        $scope.create_form = !$scope.create_form;
    }

    $scope.displayFormStatus = function () {
        // alert("display status: " + $scope.create_form);
        return $scope.create_form;
    };

    $scope.hideOrShow = function(item) {
        // alert(item.parentid);
        if(item.parentId) {
            return true;
        }
        return false;
        // if(parent != 0) {
        //     alert("Hide parent")
        //     return true;
        // }
        // return false;
    }

    $scope.getImageContent = function(item) {
        var data;
        if(item.displayImage.length > 0) {
            // data = "data:" + item.displayImage[0].contentType + ";base64," + item.displayImage[0].content;
            data = item.displayImage[0];
        } else {
            data = "#";
        }
        return data;
    }

    $scope.getParent = function(item) {
        // alert('Looking for paren ' + item.parentId);
        for (var i = 0; i < $scope.items.length; i++) {
            if($scope.items[i]._id == item.parentId) {
                return " " + $scope.items[i].name;
            }
        }
    }

    $scope.linkTOAddCategory = function() {
        $location.path("/categories/new");
    }

    $scope.deleteCategory = function(item) {
        if(confirm("Do you want to delete category with name: " + item.name + " ?")) {
            $http.delete($scope.url + "/" + item._id).success(function(data, status){
                $scope.items = data;
            }).error(function(status, response){
                alert("Failed to delete requested category");
            });
        }
    }
}

var FoodItemsAddCtrl = function($scope, $location, $http) {
    $scope.food_items_url = "/api/food_items";
    $scope.categories_url = "/api/categories";
    $scope.availability = true;
    // $scope.foodItemsForm = {name: '', description: '', price: '', categoryId: '', display_image: '', android_image: ''};

    $scope.getCategories = function() {
        $http.get($scope.categories_url).success(function(data, status) {
            $scope.categories = data;
        }).error(function(status, response) {
            alert('Failed to retrive categories due to ' + response);
        });
    }
    $scope.getCategories();

    $scope.addFoodItem = function() {
        //alert('Adding food item ' + $scope.foodItemsForm.name);
        $http.post($scope.url, $scope.foodItemsForm).success(function(data, status) {
            $scope.foodItems = data;
        }).error(function(data, status) {
            // alert("Request failed: " + status);
        });
    }
};

var FoodItemsEditCtrl = function($scope, $location, LoungeAjax, $http) {
    $scope.subcat = false;
    $scope.cat_id = $('#cat_id').val();
    $scope.food_id = $('#food_id').val();
    $scope.food_items_url = "/api/food_items";
    $scope.categories_url = "/api/categories";
    $scope.foodItemsForm = [{name: '', description:'', displayImage:'', availability: false, androidImage: '', price: '', categoryId:''}];

    $scope.getCategories = function() {
        $http.get($scope.categories_url).success(function(data, status) {
            $scope.categories = data;
            for (var i = 0; i < data.length; i++) {
                if($scope.cat_id == data[i]._id) {
                    $scope.currentCategory = data[i];
                    break;
                }
            }
        }).error(function(status, response) {
            alert('Failed to retrive categories due to ' + response);
        });
    }
    $scope.getCategories();

    $scope.getFoodItems = function() {
        $http.get($scope.food_items_url + '/' + $scope.food_id).success(function(data, status) {
            $scope.foodItem = data;
            $scope.foodItemsForm.name = $scope.foodItem[0].name;
            $scope.foodItemsForm.description = $scope.foodItem[0].description;
            $scope.foodItemsForm.displayImage = $scope.foodItem[0].displayImage;
            $scope.foodItemsForm.androidImage = $scope.foodItem[0].androidImage;
            $scope.foodItemsForm.price = $scope.foodItem[0].price;
            $scope.foodItemsForm.categoryId = $scope.foodItem[0].categoryId;
            $scope.foodItemsForm.availability = $scope.foodItem[0].availability;
        }).error(function(status, response) {
            alert('Failed to retrive food items due to ' + response);
        });
    }
    $scope.getFoodItems();

    $scope.updateFoodItem = function() {
        $scope.foodItemsForm.categoryId = $scope.currentCategory._id;
        $http.post($scope.url, $scope.foodItemsForm).success(function(data, status) {
            $scope.foodItems = data;
        }).error(function(data, status) {
            // alert("Request failed: " + status);
        });
    }

    // $scope.identifySelected = function() {
    //     var value = $scope.cat_id;
    //     alert('Identifying slected ' + cat_id)
    //     $('#' + value).attr('selected', 'selected');
    //     // if(item._id == $scope.foodItemsForm.categoryId) {
    //     //     // alert("_id: " + item._id + " cat id: " + $scope.foodItemsForm.categoryId);
    //     //     return 'selected';
    //     // }
    // }
};

var FoodItemsCtrl = function($scope, $location, $http) {
    $scope.subcat = false;
    $scope.cat = "Categories List";
    $scope.food_items_url = "/api/food_items";
    $scope.categories_url = "/api/categories";
    $scope.filters = [
        { name: 'Alphabetical', value: 'Alphabetical'},
        { name: 'Categories', value: 'Categories'}
    ];
    // $scope.displayFilter = $scope.filters[0];

    $scope.getCategories = function() {
        $http.get($scope.categories_url).success(function(data, status) {
            $scope.categories = data;
        }).error(function(status, response) {
            alert('Failed to retrive categories due to ' + response);
        });
    }
    $scope.getCategories();

    $scope.getFoodItems = function() {
        $http.get($scope.food_items_url).success(function(data, status) {
            $scope.foodItems = data;
        }).error(function(status, response) {
            alert('Failed to retrive food items due to ' + response);
        });
    }
    $scope.getFoodItems();

    $scope.getImageContent = function(item) {
        var data;
        if(item.displayImage.length > 0) {
            // data = "data:" + item.displayImage[0].contentType + ";base64," + item.displayImage[0].content;
            data = item.displayImage[0];
        } else {
            data = "#";
        }
        return data;
    }

    $scope.getCategory = function(item) {
        for (var i = 0; i < $scope.categories.length; i++) {
            if($scope.categories[i]._id == item.categoryId) {
                return " " + $scope.categories[i].name;
            }
        }
    }

    $scope.linkTOAddFoodItem = function() {
        $location.path("/food_items/new");
    }

    $scope.deleteFoodItem = function(foodItem) {
        if(confirm("Do you want to delete food food item with name: " + foodItem.name + " ?")) {
            $http.delete($scope.food_items_url + "/" + foodItem._id).success(function(data, status){
                $scope.foodItems = data;
            }).error(function(status, response){
                alert("Failed to delete requested category" + response);
            });
        }
    }
};

var TablesCtrl = function($scope, $location) {
    $scope.cat = "Tables List"
}

var OrdersCtrl = function($scope, $location) {
    $scope.cat = "Orders List"
}
