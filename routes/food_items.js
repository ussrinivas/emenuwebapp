var async = require('async');
var fs = require('fs');

var FoodItem, Category, Image;

// Helpers

var saveImage = function(name, path, contentType) {
    var image = new Image({
        name: name,
        content: fs.readFileSync(path),
        contentType: contentType
    });
    image.save();
    return image._id;
};

var saveAndroidImages = function(files, foodItem) {
    async.parallel([
        function(callback) {
            if(files && files.android_image1 && files.android_image1.size > 100) {
                foodItem.androidImage1 = saveImage(files.android_image1.name, files.android_image1.path, files.android_image1.contentType);
            }
            callback(null, 1);
        },
        function(callback) {
            if(files && files.android_image2 && files.android_image2.size > 100) {
                foodItem.androidImage2 = saveImage(files.android_image2.name, files.android_image2.path, files.android_image2.contentType);
            }
            callback(null, 2);
        },
        function(callback) {
            if(files && files.android_image3 && files.android_image3.size > 100) {
                foodItem.androidImage3 = saveImage(files.android_image3.name, files.android_image3.path, files.android_image3.contentType);
            }
            callback(null, 3);
        },
        function(callback) {
            if(files && files.android_image4 && files.android_image4.size > 100) {
                foodItem.androidImage4 = saveImage(files.android_image4.name, files.android_image4.path, files.android_image4.contentType);
            }
            callback(null, 4);
        }],
        function(err, result) {

        }
    );

    // if(files && (files.android_image1 || files.android_image2 || files.android_image3 || files.android_image4)) {
    //     FoodItem.update({_id: foodItem._id},
    //         {androidimage1: foodItem.androidimage1, android_image2: foodItem.android_image2,
    //             android_image3: foodItem.android_image3, android_image4:foodItem.android_image4},
    //         function(err) {
    //             if(err) throw err;
    //         });
    // }
}

// Rendering food_items

// GET /food_items
var index = function(req, res) {
    res.render('food_items', {
        layout: 'layout_fooditems',
        title: 'Lounge eMenu: Food Items'
    });
};

// GET /food_items/new
var add = function(req, res) {
    res.render('food_items_new', {
        layout: 'layout_fooditems',
        title: 'Lounge eMenu: Food Items'
    });
}

// GET /food_items/edit/:name
var edit = function(req, res) {
    var categories;
    Category.find(function(err, docs){
        categories = docs;
    });
    FoodItem.find({name: req.params.name},
        function(err, foodItems) {
            var json = foodItems[0];
            // console.log('Item to modify: ' + json);
            res.render('food_items_edit', {
                layout: "layout_fooditems",
                title: 'Lounge eMenu: Food Items',
                categories: categories,
                food_id: json._id,
                cat_id: json.categoryId,
                food_name: json.name});
        }
    );
}

// API's

// GET /api/food_tems

var getFoodItems = function(req, res) {
    // console.log('Fetching food items');
    if (FoodItem) {
        FoodItem.find(function(err, foodItems) {
            if (err) {
                res.send('Failed to retrieve food items');
                console.log(err);
            } else {
                res.type('application.json');
                res.write(JSON.stringify(foodItems));
                res.end();
            }
        });
    } else {
        res.send('Food items model can not be retrieved...');
    }
}

// POST /api/food_items
var createFoodItem = function(req, res) {
    var content = req.body;
    // console.log(content);
    var b = req.body;
    var f = req.files;

    // console.log('Creating food item with ' + JSON.stringify(b));
    // console.log('Creating food item with ' + JSON.stringify(f));

    // console.log('parent id: ' + b.categoryId);
    Category.find({
        _id: b.categoryId
    }, function(err, category) {
        if (err) {

        } else {
            // console.log('Category: ' + category[0]);
            var foodItem = new FoodItem({
                name: b.name,
                description: b.description,
                categoryId: category[0]._id,
                price: parseInt(b.price, 10)
            });
            if(b.availability) {
                foodItem.availability = true;
            } else {
                foodItem.availability = false;
                // Check if all the food items in this category been disabled, disable this catgory also
            }
            async.series([
                function(callback) {
                    saveAndroidImages(f, foodItem);
                    callback(null, 1);
                },
                function(callback) {
                    if(f && f.display_image && f.display_image.size > 100) {
                        foodItem.displayImage = saveImage(f.display_image.name, f.display_image.path, f.display_image.type);
                    }
                    callback(null, 2);
                },
                function(callback) {
                    console.log('New Food Item: ' + foodItem);
                    foodItem.save(function(err) {
                        if(!err) {
                            if(foodItem.availability) {
                                Category.update({_id: category[0]._id}, {availability: true}, function(err){
                                    if(err) {
                                        console.log('Failed to update category');
                                        throw err;
                                    }
                                });
                            }
                            res.redirect('/food_items');
                        }
                    });
                }
            ],
            function(err, result) {
            }
            );
        }
    })

}

// /api/food_items/:id
var deleteFoodItem = function(req, res) {
    var id = req.params.id;
    if(FoodItem) {
        FoodItem.remove({_id: id}, function(err, docs) {
            if(err) throw err;
            getFoodItems(req, res);
        });
    }
}

// /api/food_items
var updateFoodItem= function(req, res) {
    var json = eval('(' + JSON.stringify(req.body) + ')');
    var category;
    var currentCategory;
    var files = req.files;

// check for images

    var foodItem = new FoodItem({
        name: json.name,
        description: json.description,
        categoryId: json.categoryId,
        price: json.price
    });

    if(json.availability) {
        foodItem.availability = true;
    } else {
        foodItem.availability = false;
    }

    Category.find(function(err, docs) {
        currentCategory = docs[json.categoryId];
        foodItem.categoryId = currentCategory._id;
        console.log('Category info: ' + foodItem);
        console.log('Current category: ' + currentCategory.name);

        async.series([function(callback) {
            if(files) {
                if(files.display_image && files.display_image.size > 100) {
                    var oldDisplayImage;
                    var newDisplayImage = saveImage(files.display_image.name, files.display_image.path, files.display_image.contentType);
                    FoodItem.find({_id: json.food_id}, function(err, docs) {
                        if(err) throw err;
                        oldDisplayImage = docs[0].displayImage[0];
                    });
                    FoodItem.update({_id: json.food_id}, {displayImage: newDisplayImage}, function(err) {
                        if(err) throw err;
                    });
                    Image.remove({_id: oldDisplayImage}, function(err, docs) {
                        if(err) throw err;
                    });
                }
                if(files.android_image1 && files.android_image1.size > 100) {
                    var oldDisplayImage;
                    var newDisplayImage = saveImage(files.android_image1.name, files.android_image1.path, files.android_image1.contentType);
                    FoodItem.find({_id: json.food_id}, function(err, docs) {
                        if(err) throw err;
                        oldDisplayImage = docs[0].androidImage1[0];
                    });
                    FoodItem.update({_id: json.food_id}, {androidImage1: newDisplayImage}, function(err) {
                        if(err) throw err;
                    });
                    Image.remove({_id: oldDisplayImage}, function(err, docs) {
                        if(err) throw err;
                    });
                }
                if(files.android_image2 && files.android_image2.size > 100) {
                    var oldDisplayImage;
                    var newDisplayImage = saveImage(files.android_image2.name, files.android_image2.path, files.android_image2.contentType);
                    FoodItem.find({_id: json.food_id}, function(err, docs) {
                        if(err) throw err;
                        oldDisplayImage = docs[0].androidImage2[0];
                    });
                    FoodItem.update({_id: json.food_id}, {androidImage2: newDisplayImage}, function(err) {
                        if(err) throw err;
                    });
                    Image.remove({_id: oldDisplayImage}, function(err, docs) {
                        if(err) throw err;
                    });
                }
                if(files.android_image3 && files.android_image3.size > 100) {
                    var oldDisplayImage;
                    var newDisplayImage = saveImage(files.android_image3.name, files.android_image3.path, files.android_image3.contentType);
                    FoodItem.find({_id: json.food_id}, function(err, docs) {
                        if(err) throw err;
                        oldDisplayImage = docs[0].androidImage3[0];
                    });
                    FoodItem.update({_id: json.food_id}, {androidImage3: newDisplayImage}, function(err) {
                        if(err) throw err;
                    });
                    Image.remove({_id: oldDisplayImage}, function(err, docs) {
                        if(err) throw err;
                    });
                }
                if(files.android_image4 && files.android_image4.size > 100) {
                    var oldDisplayImage;
                    var newDisplayImage = saveImage(files.android_image4.name, files.android_image4.path, files.android_image4.contentType);
                    FoodItem.find({_id: json.food_id}, function(err, docs) {
                        if(err) throw err;
                        oldDisplayImage = docs[0].androidImage4[0];
                    });
                    FoodItem.update({_id: json.food_id}, {androidImage4: newDisplayImage}, function(err) {
                        if(err) throw err;
                    });
                    Image.remove({_id: oldDisplayImage}, function(err, docs) {
                        if(err) throw err;
                    });
                }
            }
            callback(null, 2);
        },
        function(callback) {
            FoodItem.update({_id: json.food_id},
                {name: json.name,
                description: json.description,
                categoryId: currentCategory._id,
                price: json.price,
                availability: foodItem.availability},
                function(err) {
                    if (err) {
                        throw err;
                    }
                    if(foodItem.availability) {
                        console.log('availability is true');
                        Category.update({_id: currentCategory._id}, {availability: true}, function(err){});
                    } else {
                        console.log('availability is false');
                        FoodItem.find({categoryId: currentCategory._id, availability: true}, function(err, foods) {
                            console.log(foods);
                            if(foods && foods.length > 0) {
                                console.log('Making category true');
                                Category.update({_id: currentCategory._id}, {availability: true}, function(err){});
                            } else {
                                console.log('No food items');
                                Category.update({_id: currentCategory._id}, {availability: false}, function(err){});
                            }
                        });
                    }
                    res.redirect("/food_items");
                }
            );
        }],
        function(err, result) {
        }
        );
    });
}

// GET /api/food_items/:id
var getFoodItemById = function(req, res) {
    var id = req.params.id;
    if(FoodItem) {
        FoodItem.find({_id: id},
            function(err, docs) {
                if(err) throw err;
                res.type('application.json');
                res.write(JSON.stringify(docs));
                res.end();
            }
        );
    }
}

module.exports = function(app, f, c, i) {
    FoodItem = f;
    Category = c;
    Image = i;
    app.get('/food_items', index);
    app.get('/food_items/new', add);
    app.get('/food_items/edit/:name', edit);
    app.get('/api/food_items', getFoodItems);
    app.get('/api/food_items/:id', getFoodItemById);
    app.post('/api/food_items', createFoodItem);
    app.delete('/api/food_items/:id', deleteFoodItem);
    app.put('/api/food_items', updateFoodItem);
    // app.get('/api/food_tems/:id', getById);
    // app.get('/api/food_tems/name/:name', getByName);

}