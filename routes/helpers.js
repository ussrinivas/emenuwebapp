var saveImage = function(name, path, contentType) {
    var image = new Image({
        name: name,
        content: fs.readFileSync(path),
        contentType: contentType
    });
    image.save();
    return image._id;
};