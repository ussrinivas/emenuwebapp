var async = require('async');
var fs = require('fs');


var db, Category, Image, FoodItem;

// GET /api/categories

var getCategories = function(req, res) {
    // console.log('Fetching categories');
    if (Category) {
        Category.find(function(err, categories) {
            if (err) console.log(err);
            else {
                console.log(categories)
                res.type('application/json');
                res.write(JSON.stringify(categories));
                res.end();
            }
        });
    } else {
        console.log('db is null');
    }
};

var getById = function(req, res) {
    var id = req.params.id;
    if (Category) {
        Category.find({
            _id: id
        }, function(err, rows) {
            if (err) throw err;
            res.type('application/json');
            res.write(JSON.stringify(rows));
            res.end();
        });
    }
};

var getByName = function(req, res) {
    var name = req.params.name;
    if (db) {
        db.query("select * from category where name=" + name, function(err, rows, fields) {
            if (err) throw err;
            res.type('application/json');
            res.write(JSON.stringify(rows));
            res.end();
        });
    }
};

var saveImage = function(name, path, contentType) {
    var image = new Image({
        name: name,
        content: fs.readFileSync(path),
        contentType: contentType
    });
    image.save();
    return image._id;
};

var saveCategory = function(req, res) {
    var content = req.body;
    var files = req.files;
    var parentCat;
    var json = eval('(' + JSON.stringify(content) + ')');

    console.log('Received content: ' + JSON.stringify(content));

    var category = new Category({
        name: json.name,
        description: json.description,
        displayOrder: json.displayOrder});

    if(json.parentId && json.parentId != '') {
        category.typeId = 2;
        console.log('Parent is there, so you need to assign it ' + json.parentId );
        Category.find({_id: json.parentId}, function(err, docs) {
            if(err) throw err;
            console.log('Got id ' + docs[0]._id);
            category.parentId = docs[0]._id;
            console.log('After assigning category status : ' + category);
            if( files && files.display_image && files.display_image.size > 100 ) {
                category.displayImage = saveImage(files.display_image.name, files.display_image.path, files.display_image.type);
            }

            if( files && files.android_image && files.android_image.size > 100 ) {
                category.androidImage = saveImage(files.android_image.name, files.android_image.path, files.android_image.type);
            }
            console.log(category);
            category.availability = false;
            category.save(function(err) {
                if (err) throw err;
                res.redirect('/categories');
                // a.img.data = fs.readFileSync(imgPath);
            });
        });
    } else {
        category.typeId = 1;
        if( files && files.display_image && files.display_image.size > 100 ) {
            category.displayImage = saveImage(files.display_image.name, files.display_image.path, files.display_image.type);
        }

        if( files && files.android_image && files.android_image.size > 100 ) {
            category.androidImage = saveImage(files.android_image.name, files.android_image.path, files.android_image.type);
        }
        console.log(category);
        category.availability = false;
        category.save(function(err) {
            if (err) throw err;
            res.redirect('/categories');
            // a.img.data = fs.readFileSync(imgPath);
        });
    }
};

var deleteCategory = function(req, res) {
    var id = req.params.id;
    if (Category) {
        Category.remove({_id: id}, function(err, docs) {
            if (err) {
                throw err;
            }
            FoodItem.remove({categoryId: id}, function(err, docs) {});
            getCategories(req, res);
        });
    }
};

var editCategory = function(req, res) {
    Category.find({
        name: req.params.name
    }, function(err, categories) {
        if (err) throw err;
        var json = categories[0];
        res.render('categories_edit', {
            layout: "layout_category",
            title: 'Lounge eMenu: Edit Category',
            items: categories,
            cat_id: json._id,
            cat_name: json.name
        });
    });
};

var index = function(req, res) {
    res.render('categories', {
        layout: 'layout_category',
        title: 'Lounge eMenu: Categories'
    });
};

// var saveCat = function(req, res) {
//     console.log(req.files.display_image);
//     console.log(req.files.android_image);
//     console.log(req.files);
//     // saveCategory(req, res);
//     exports.index(req, res);
// }

var updateCategory = function(req, res) {
    var json = eval('(' + JSON.stringify(req.body) + ')');

    if (Category) {
        Category.update({
            _id: json.cat_id
        }, {
            name: json.name,
            description: json.description,
            displayOrder: json.displayOrder
        },

        function(err) {
            if (err) {
                throw err;
            }
            res.redirect("/categories");
        });
    }
}

var add = function(req, res) {
    res.render('categories_new', {
        layout: 'layout_category',
        title: 'Lounge eMenu: Categories',
        layout: 'layout_category'
    });
};

var fetchImage = function(req, res) {
        Image.find({
            _id: req.params.id
        }, function(err, images) {
            if (err) {
                console.log(err);
                throw err;
            }
            else {
                if(images[0].contentType == "") {
                    res.type("application/png");
                }
                res.write(images[0].content);
                res.end();
            }
        });
}

module.exports = function(app, c, i, f) {
    Category = c;
    Image = i;
    FoodItem = f;
    app.get('/categories', index);
    app.get('/categories/new', add);
    app.get('/categories/edit/:name', editCategory);
    app.get('/api/categories', getCategories);
    app.put('/api/categories', updateCategory);
    app.post('/api/categories', saveCategory);
    app.get('/api/categories/:id', getById);
    app.get('/api/categories/name/:name', getByName);
    app.delete('/api/categories/:id', deleteCategory);
    app.get('/api/image/:id', fetchImage);
}