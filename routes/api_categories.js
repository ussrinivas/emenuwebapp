var db;

var get = function(req, res) {
    console.log("Received request for get");
    if (db) {
        console.log('Execurint db');
        db.query("select * from category", function(err, rows, fields) {
            if (err) throw err;
            res.type('application.json');
            res.write(JSON.stringify(rows));
            res.end();
        });
    } else {
        console.log('db is null');
    }
};

var getById = function(req, res) {
    var id = req.params.id;
    if (db) {
        db.query("select * from category where id=" + id, function(err, rows, fields) {
            if (err) throw err;
            res.type('application.json');
            res.write(JSON.stringify(rows));
            res.end();
        });
    }
};

var saveCategory = function(req, res) {
    var content = req.body;
    console.log(content);
    var json = eval('(' + JSON.stringify(content) + ')');
    // console.log('Received data: ' + json);
    // console.log('Content is : ' + json.name + ' desc: ' + json.desc_c + ' typeid: ' + json.typeid + ' parentid: ' + json.parentid);
    db.query("insert into category(name, desc_c, imgid, typeid, parentid) values(?,?,?,?,?)", [json.name, json.desc_c, req.files.display_image.path, json.typeid, json.parentid], function(err, info) {
        if (err) throw err;
        get(req, res);
    });
};

var deleteCategory = function(req, res) {
    var id = req.params.id;
    if (db) {
        db.query("delete from category where id=" + id, function(err, rows, fields) {
            if (err) throw err;
            get(req, res);
        });
    }
}

module.exports = function(app, database) {
    db = database;
    app.get('/api/categories', get);
    app.get('/api/categories/:id', getById);
    app.delete('/api/categories/:id', deleteCategory);
    app.post('/api/categories', saveCategory);
}