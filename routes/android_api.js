var Category, Image, FoodItems;

// result: 2 error, 1: success

var getCategories = function(req, res) {
    // Category.find({"parentId": { "$exists" : false }, availability: true}, function(err, docs) {
    Category.find( {query: {"parentId": { "$exists" : false }, availability: true}, $orderby: { displayOrder : 1 }}, function(err, docs) {
        res.type("application/json");
        if(err) {
            console.log(err);
            res.send("{result: 2}");
        } else {
            res.send("{result: 1,\n categories:[" + docs + "]}");
        }
    });
};

var getCategoriesByParent = function(req, res) {
    var parentId = req.params.parent;
    Category.find({parentId: parentId}, function(err, docs) {
        res.type("application/json");
        if(err) {
            res.send("{result: 2}");
        } else {
            res.send("{result: 1,\n categories:[" + docs + "]}" );
        }
    });
};

var getFoodItems = function(req, res) {
    var parentId = req.params.parent;
    FoodItems.find({categoryId: parentId, availability: true}, function(err, docs) {
        console.log(docs);
        res.type("application/json");
        if(err) {
            res.send("{result: 2}");
        } else {
            res.send("{result: 1,\n food_items:[" + docs + "]}" );
        }
    });
};

module.exports = function(app, f, c, i) {
    Category = c;
    Image = i;
    FoodItems = f;
    app.get('/api/android/categories', getCategories);
    app.get('/api/android/categories/:parent', getCategoriesByParent);
    app.get('/api/android/food_items/:parent', getFoodItems);
}
