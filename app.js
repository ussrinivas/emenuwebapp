
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , http = require('http')
  , mongoose = require('mongoose')
  , path = require('path');

var app = express();
// var db = mysql.createConnection({ host: 'localhost', user: 'root', password: 'rootuser', database: 'lounge'});
var db = mongoose.connect('mongodb://localhost/MysoreLoungeApp');
var Schema = mongoose.Schema;

var ImageModel = new Schema({
    name: String,
    content: {
        type: Buffer,
        enum: ['thumbnail', 'catalog', 'detail', 'zoom'],
        required: true
    },
    contentType: String
});

var CategorySchema = new Schema({
  name: String,
  description: String,
  displayOrder: Number,
  availability: Boolean,
  childCount: Number,
  parentId: { type : Schema.ObjectId, ref : 'Category' },
  typeId: Number,
  androidImage: [{type: Schema.ObjectId, ref: 'Image'}],
  displayImage: [{type: Schema.ObjectId, ref: 'Image'}]
});

CategorySchema.path('name').validate(function(a) {
  return a.length > 0
}, 'Category name can not be blank');

CategorySchema.path('description').validate(function(a) {
  return a.length > 0
}, 'Category description can not be blank');

var FoodItemsSchema = new Schema({
  name: { type: String },
  availability: Boolean,
  description: { type: String, required: true },
  categoryId: { type : Schema.ObjectId, ref : 'Category' },
  price: { type: Number, required: true },
  displayImage: [{type: Schema.ObjectId, ref: 'Image'}],
  androidImage1: [{type: Schema.ObjectId, ref: 'Image'}],
  androidImage2: [{type: Schema.ObjectId, ref: 'Image'}],
  androidImage3: [{type: Schema.ObjectId, ref: 'Image'}],
  androidImage4: [{type: Schema.ObjectId, ref: 'Image'}]
});

FoodItemsSchema.path('name').validate(function(a) {
  return a.length > 0
}, 'Food item name can not be blank');

FoodItemsSchema.path('description').validate(function(a) {
  return a.length > 0
}, 'Food item description can not be blank');

FoodItemsSchema.path('price').validate(function(a) {
  return a > 0
}, 'Food item price can not be 0');

var Image = mongoose.model('Image', ImageModel);
var FoodItem = mongoose.model('FoodItem', FoodItemsSchema);
var Category = mongoose.model('Category', CategorySchema);

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  // app.use(express.bodyParser({keepExtensions: true, uploadDir:path.join(__dirname, '/images')}));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser('your secret here'));
  app.use(express.session());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

var categories = require('./routes/categories')(app, Category, Image, FoodItem);
var food_items = require('./routes/food_items')(app, FoodItem, Category, Image);
var android_apis = require('./routes/android_api')(app, FoodItem, Category, Image);

app.get('/', routes.index);
app.get('/home', routes.index);
// app.get('/images/:name', function(req, res) {
//     res.sendfile('./images/' + req.params.name);
// });
app.get('/api/image/', function(req, res) {
    res.sendfile('./public/images/no_image.gif');
});

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
